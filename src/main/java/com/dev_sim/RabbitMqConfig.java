package com.dev_sim;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
public class RabbitMqConfig {

    @Value("${simulator.rabbitmq.caching.connection.factory.hostname}")
    private String hostname;

    @Value("${simulator.rabbitmq.readings.exchange}")
    private String readingsExchange;
    @Value("${simulator.rabbitmq.readings.queue}")
    private String readingsQueue;
    @Value("${simulator.rabbitmq.readings.routing_key}")
    private String readingsRoutingKey;

    @Bean
    public CachingConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(hostname);
    }

    @Bean
    public RabbitAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        return new RabbitTemplate(connectionFactory());
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(readingsQueue())
                .to(readingsExchangeTopic())
                .with(readingsRoutingKey);
    }

    @Bean
    public TopicExchange readingsExchangeTopic() {
        return new TopicExchange(readingsExchange);
    }

    @Bean
    public Queue readingsQueue() {
        return new Queue(readingsQueue);
    }

}
