FROM openjdk:17.0.1
VOLUME /tmp
EXPOSE 8446
ARG JAR_FILE
COPY ${JAR_FILE} dev_sim.jar
COPY src ./src
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","/dev_sim.jar"]